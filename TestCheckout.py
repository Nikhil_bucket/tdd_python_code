import pytest
from unittest.mock import MagicMock
from Checkout import Checkout

lineCnt = 0

@pytest.fixture()
class testCheckout():
  def checkout(self):
      self.checkout = Checkout()
      checkout.addItemPrice("a", 1)
      checkout.addItemPrice("b", 2)
      return checkout

  def test_canCalculateTotal(self,checkout):
      self.checkout.addItem("a")
      assert checkout.calculateTotal() == 1

  def test_getCorrectTotalWithMultipleItems(self,checkout):
      self.checkout.addItem("a")
      self.checkout.addItem("b")
      assert checkout.calculateTotal() == 3

  def test_canApplyDiscountRule(self,checkout):
      self.checkout.addDiscount("a", 3, 2)
      self.checkout.addItem("a")
      self.checkout.addItem("a")
      self.checkout.addItem("a")
      assert checkout.calculateTotal() == 2

  def test_exceptionWithBadItem(self,checkout):
      with pytest.raises(Exception):
          self.checkout.addItem("c")

  def test_verifyReadPricesFile(self,checkout, monkeypatch):
      self.mock_file = MagicMock()
      self.mock_file.__enter__.return_value.__iter__.return_value = ["c 3"]
      self.mock_open = MagicMock(return_value = mock_file)
      monkeypatch.setattr("builtins.open", mock_open)
      checkout.readPricesFile("testfile")
      checkout.addItem("c")
      result = checkout.calculateTotal()
      mock_open.assert_called_once_with("testfile")
      assert result == 3